import {userContext} from './MyContext';
import './App.css';
import Profile from './components/Profile';
import react from 'react';

class App extends react.Component {

  state = {
    user : {
      name:"lisa",
      age:8,
    }
  }

render(){

  return (
    <div>
    <userContext.Provider value={this.state.user}>
     <Profile />
    </userContext.Provider>
    </div>
  );
}
}

export default App;
