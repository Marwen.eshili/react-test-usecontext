import {userContext} from '../MyContext';

import React ,{useContext} from 'react'

function ContentData() {

    const user = useContext(userContext);
    return (
        <div className="card m-5">
            <div className="card-body ">
                <h1>name :  {user.name}</h1>
                    <h1>age : {user.age}</h1>
            </div>
        </div>

        // <userContext.Consumer>
        //     {
        //         user => {
        //             return(
        //                 <div className="card m-5">
        //                     <div className="card-body ">
        //                          <h1>name :  {user.name}</h1>
        //                         <h1>age : {user.age}</h1>
        //                      </div>
        //                 </div>
        //             )
        //         }
        //     }
        // </userContext.Consumer>
      
    )
}

export default ContentData
